#include <libudev.h>
#include "csapp.h"

static struct udev_device *obtener_hijo(struct udev*, struct udev_device*, const char*);
static void monitorear_disp_alm_masivo(struct udev*);

int main(int argc, char **argv)
{
	struct udev *udev;

	/* Create the udev object */
	udev = udev_new();
	if (!udev) {
		printf("Can't create udev\n");
		exit(1);
	}

	monitorear_disp_alm_masivo(udev);

	udev_unref(udev);

	return 0;

}

static void monitorear_disp_alm_masivo(struct udev* udev)
{
	struct udev_monitor *mon;
	struct udev_device *dev;
	int fd;

	/* Crea un monitor de dispositivos usb */
	mon = udev_monitor_new_from_netlink(udev, "udev");
	udev_monitor_filter_add_match_subsystem_devtype(mon, "scsi", "scsi_device");
	udev_monitor_enable_receiving(mon);

	/* Retorna un descriptor de archivos que luego será utilizado con select */
	fd = udev_monitor_get_fd(mon);

	while (1) {
		fd_set fds;
		struct timeval tv;
		int ret;
		
		FD_ZERO(&fds);
		FD_SET(fd, &fds);
		tv.tv_sec = 0;
		tv.tv_usec = 0;
		
		ret = select(fd+1, &fds, NULL, NULL, &tv);
		/* Revisa si el descriptor de archivo ha recibido datos. */
		if (ret > 0 && FD_ISSET(fd, &fds)) {
			printf("\nselect() says there should be data\n");
			
			/* Recibe el dispositivo del monitor,
			   select() se aseguró que esta llamada no bloquearía. */
			dev = udev_monitor_receive_device(mon);
			
			if (dev) {
				struct udev_device* block = obtener_hijo(udev, dev, "block");
				struct udev_device* usb = udev_device_get_parent_with_subsystem_devtype(dev,"usb","usb_device");

				if(block) {
					if(usb) {
						printf("Got Mass Storage USB Device\n");
						printf("   Node: %s\n", udev_device_get_devnode(block));
						printf("   Subsystem: %s\n", udev_device_get_subsystem(block));
						printf("   Devtype: %s\n", udev_device_get_devtype(block));
						printf("   Action: %s\n", udev_device_get_action(dev));
						printf("   USB ID: %s:%s\n", udev_device_get_sysattr_value(usb, "idVendor"),
													udev_device_get_sysattr_value(usb, "idProduct"));
						udev_device_unref(usb);
					}
					udev_device_unref(block);
				}else {
					printf("   Node: %s\n", udev_device_get_devnode(dev));
					printf("   Subsystem: %s\n", udev_device_get_subsystem(dev));
					printf("   Devtype: %s\n", udev_device_get_devtype(dev));
					printf("   Action: %s\n", udev_device_get_action(dev));
				}
				
				udev_device_unref(dev);
			}
			else {
				printf("No Device from receive_device(). An error occured.\n");
			}					
		}
		usleep(250*1000);
		printf(".");
		fflush(stdout);
	}
}

static struct udev_device *obtener_hijo(struct udev* udev, struct udev_device* padre, const char* subsistema)
{
	struct udev_device* hijo = NULL;
	struct udev_enumerate *enumerar = udev_enumerate_new(udev);

	udev_enumerate_add_match_parent(enumerar, padre);
	udev_enumerate_add_match_subsystem(enumerar, subsistema);
	udev_enumerate_scan_devices(enumerar);

	struct udev_list_entry *dispositivos = udev_enumerate_get_list_entry(enumerar);
	struct udev_list_entry *entrada;

	udev_list_entry_foreach(entrada, dispositivos) {
		const char *ruta = udev_list_entry_get_name(entrada);
		hijo = udev_device_new_from_syspath(udev, ruta);
		break;
	}

	udev_enumerate_unref(enumerar);
	return hijo;
}